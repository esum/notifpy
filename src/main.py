#!/usr/bin/python3

import configparser
import os
import socket
import subprocess
import threading


def notify(conn, buffer, notifier):
	message = conn.recv(buffer).decode('utf-8')
	subprocess.run([notifier, message])
	conn.close()


if __name__ == '__main__':
	config = configparser.ConfigParser()
	config.read('notifpy.ini')
	try:
		os.remove(config['notifpy']['socket'])
	except OSError:
		pass
	sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
	sock.bind(config['notifpy']['socket'])
	sock.listen(int(config['notifpy']['listen']))
	while 1:
		conn, addr = sock.accept()
		threading.Thread(
			target=notify,
			args=(conn, int(config['notifpy']['buffer']), config['notifpy']['notifier'])
		).start()

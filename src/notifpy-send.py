#!/usr/bin/python3

import socket
import sys


if __name__ == '__main__':
	sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
	sock.connect('/tmp/notifpy.sock')
	sock.send(sys.argv[1].encode('utf-8'))

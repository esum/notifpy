# notifpy

Serveur local d'envoi de notification

## Dépendances

 * Unix OS (Linux and macOS tested)
 * `python` (>=3.5)
 * notify-send (optionnel) (fourni par: libnotify-bin sous Debian, libnotify sous ArchLinux)

## Configuration

```ini
[notifpy]
; Chemin de la socket unix
socket = /tmp/notifpy.sock
; Nombre de notification simultanées maximal
listen = 2
; Taille du buffer de réception
buffer = 1024
; Binaire utilisé pour notifier (le contenu de la notification est passé en argument)
notifier = notify-send
```

## Utilisation

Lancer main.py

Vous pouvez tester avec `notifpy-send.py` :
```bash
notifpy-send.py "Hello, World!"
```

Pour une utilisation via SSH :
```bash
ssh user@host rm /tmp/notifpy.sock ; ssh -R /tmp/notifpy.sock:/tmp/notifpy.sock user@host
```

Pour une utilisation sous WeeChat :
```
/trigger addoff notifpy print
/trigger set notifpy conditions ${tg_highlight} == 1 && ${tg_displayed} == 1 && ${buffer.notify} > 0
/trigger set notifpy command /exec -nosh -norc -bg python3 <PATH>/notifpy-send.py "${buffer.short_name} ${tg_prefix_nocolor}: ${tg_message}"
/trigger enable notifpy
```
(remplacer `<PATH>` par le chemin de `notifpy-send.py`)
